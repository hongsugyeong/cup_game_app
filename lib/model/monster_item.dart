class MonsterItem {
  String imgUrl;
  String charName;
  num speed;
  num hitPower;

  MonsterItem(this.imgUrl, this.charName, this.speed, this.hitPower);
}