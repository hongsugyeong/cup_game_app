class GoodsItem {
  num id;
  String imgUrl;
  String goodsTitle;
  num goodsPrice;

   GoodsItem(this.id ,this.imgUrl, this.goodsTitle, this.goodsPrice);
}