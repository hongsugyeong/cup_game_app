import 'package:cup_game_app/model/goods_item.dart';
import 'package:flutter/material.dart';

class ComponentsGoodsItems extends StatelessWidget {
  const ComponentsGoodsItems({
    super.key,
    required this.goodsItem,
    required this.callback
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
          child: Row(
            children: [
              SizedBox(
                child: Text(goodsItem.imgUrl),
              ),
              Column(
                children: [
                  Text(goodsItem.goodsTitle),
                  Text('${goodsItem.goodsPrice}'),
                ],
              )
            ],
          ),
        );
  }
}
