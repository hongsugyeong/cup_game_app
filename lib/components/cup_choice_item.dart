import 'package:flutter/material.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem({
    super.key,
    required this.pandon,
    required this.currentMoney
  });

  final num pandon; // 판돈
  final num currentMoney; // 현재 갖고 있는 돈

  @override
  State<CupChoiceItem> createState() => _CupChoiceItemState();
}

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc = 'assets/cup.png';

    // 오픈했을 경우의 조건
    //  판돈이 현재 돈보다 적을 때: bomb.png
    //  판돈이 현재 돈과 같을 때: one-dollar.png
    //  판돈이 현재 돈보다 많을 떄: two-dollar.png
    // 오픈하지 않았을 때: 무조건 cup.png

    void _calculateStart() {
      if (!_isOpen) {
        setState(() {
          _isOpen = true;
        });
      }
    }

    void _calculateImgSrc() {
      String tempImgSrc = '';
      if (_isOpen) {
        if (widget.pandon < widget.currentMoney) {
          tempImgSrc = 'assets/bomb.png';
        } else if (widget.pandon == widget.currentMoney) {
          tempImgSrc = 'assets/one-dollar.png';
        } else {
          tempImgSrc = 'assets/two-dollar.png';
        }
      } else {
        tempImgSrc = 'assets/cup.png';
      }

      setState(() {
        imgSrc= tempImgSrc;
      });
    }


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _calculateStart();
        _calculateImgSrc();
      },
      child: SizedBox(
          width: 200,
          height: 200,
          child: Image.asset(imgSrc),
      ),
    );
  }
}
