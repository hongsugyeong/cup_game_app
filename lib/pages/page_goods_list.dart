import 'package:cup_game_app/components/components_goods_item.dart';
import 'package:cup_game_app/model/goods_item.dart';
import 'package:cup_game_app/pages/page_goods_detail.dart';
import 'package:flutter/material.dart';

class PageGoodsList extends StatefulWidget {
  const PageGoodsList({super.key});

  @override
  State<PageGoodsList> createState() => _PageGoodsListState();
}

class _PageGoodsListState extends State<PageGoodsList> {
  List<GoodsItem> _list = [
    GoodsItem(1, 'assets/1.jpg', '후라이드 치킨', 100),
    GoodsItem(2, 'assets/2.jpg', '양념 치킨', 100),
    GoodsItem(3, 'assets/3.jpg', '파닭 치킨', 200),
    GoodsItem(4, 'assets/4.jpg', '후라이드 치킨', 100),
    GoodsItem(5, 'assets/5.jpg', '양념 치킨', 100),
    GoodsItem(6, 'assets/6.jpg', '파닭 치킨', 200),
    GoodsItem(7, 'assets/7.jpg', '후라이드 치킨', 100),
    GoodsItem(8, 'assets/8.jpg', '양념 치킨', 100),
    GoodsItem(9, 'assets/9.jpg', '파닭 치킨', 200),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('상품 리스트'),
      ),
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext ctx, int idx) {
          return ComponentsGoodsItems(
              goodsItem: _list[idx],
              callback: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(goodsItem: _list[idx])));
              }
          );
        },
      ),
    );
  }
}
