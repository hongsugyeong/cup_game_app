import 'package:cup_game_app/components/cup_choice_item.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  bool _isStart = false; // 시작함?
  num pandon = 100;
  List<num> result = [0, 100, 200];

  void _startGame() {
    setState(() {
      _isStart = true;
      result.shuffle();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('야바위'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_isStart) {
      return SingleChildScrollView(
        child: Column(
          children: [
            CupChoiceItem(pandon: pandon, currentMoney: result[0]),
            CupChoiceItem(pandon: pandon, currentMoney: result[1]),
            CupChoiceItem(pandon: pandon, currentMoney: result[2]),
            Container(
              child: OutlinedButton(
                onPressed: () {},
                child: const Text('종료'),
              ),
            )
          ],
        ),
      );
    } else {
      return SingleChildScrollView(
        child: Center(
          child: OutlinedButton(
            onPressed: () {
              _startGame();
            },
            child: const Text('시작'),
          ),
        ),
      );
    }
  }
}
