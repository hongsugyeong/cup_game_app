import 'package:cup_game_app/model/monster_item.dart';
import 'package:flutter/material.dart';

class PageMq extends StatefulWidget {
  const PageMq({
    super.key
  });

  @override
  State<PageMq> createState() => _PageMqState();
}

class _PageMqState extends State<PageMq> {
  MonsterItem monster1= MonsterItem('assets/bomb.png', '비빔밤', 50, 60);
  MonsterItem monster2= MonsterItem('assets/bomb.png', '머핀이', 50, 60);

  // 선공격 누구인지 계산

  // 공격했을 때 최종 공격력 몇 (크리티컬 계산식 넣기)

  // HP에 반영시키기 (setstate)

  // 죽었는지 확인하기

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('미디어쿼리 회면'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(15),
            child: Row(
              children: [
                SizedBox(
                  width: 200,
                  height: 200,
                  child: Image.asset(
                    'assets/bomb.png',
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Container(
                  width: phoneWidth - 30 - 100 - 15,
                  child: Text('붐바라비밤밤'),
                )
              ],
            ),
          )
        ],
      ),
    );

    // return SingleChildScrollView(
    //   child: SizedBox(
    //     width: phoneWidth/3,
    //     height: phoneWidth/3,
    //     child: Image.asset(
    //       'assets.bomb.png',
    //       fit: BoxFit.fill,
    //     ),
    //   ),
    // );

    // if (phoneWidth > 500) { // 랜드스케이프 버전
    //   return SingleChildScrollView(
    //     child: SizedBox(
    //       width: 200,
    //       height: 200,
    //       child: Image.asset(
    //         'assets/bomb.png',
    //         fit: BoxFit.fill,
    //       ),
    //     ),
    //   );
    // } else { // 포트레이트 버전
    //   return SingleChildScrollView(
    //     child: SizedBox(
    //       width: MediaQuery.of(context).size.width / 2,
    //       height: 200,
    //       child: Image.asset(
    //         'assets/bomb.png',
    //         fit: BoxFit.fill,
    //       ),
    //     ),
    //   );
    // }

  }
}
